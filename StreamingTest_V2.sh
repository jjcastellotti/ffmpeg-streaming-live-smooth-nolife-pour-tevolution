#!/bin/sh
 
SPAWN_DELAY=15
 
INPUT='udp://239.0.10.240:7400?fifo_size=1000000&overrun_nonfatal=1'
PUBPOINT=http://live1.tevolution.tv/nolife/nolife.isml
FFMPEG_OPTIONS="-movflags isml+frag_keyframe -f ismv -threads 0"
AUDIO_OPTIONS="-c:a libfaac -ac 2 -b:a 128k"
VIDEO_OPTIONS="-c:v libx264 -preset:v superfast -profile:v main -level:v 3.1 -coder:v 0 -g 100 -keyint_min 100 -sc_threshold 0 -r 25"
VIDEO_FILTERS="-deinterlace"
 
MAP="
  -map 0:v -b:v:0 1024k -s:v:0 640x360
  -map 0:v -b:v:1 1500k -s:v:1 1024x576
  -map 0:v -b:v:2 2500k -s:v:2 1024x576
  -map 0:a:0"
 
 
while true
do
        EVENT_ID="$(date +%Y-%m-%d-%H_%M_%S)"
        PUBPOINT_OPTIONS="/Events($EVENT_ID)/Streams(video)"

        CMD="-y -re -i $INPUT $VIDEO_FILTERS $FFMPEG_OPTIONS $AUDIO_OPTIONS $VIDEO_OPTIONS $MAP ${PUBPOINT}${PUBPOINT_OPTIONS}"

        echo ""
        echo " **** Command line                                **** "
        echo ffmpeg $CMD
        echo ""

        ffmpeg $CMD
 
        echo ""
        echo " **** Process will be respawned in $SPAWN_DELAY s **** "
        echo " ****    press Ctrl-C to abort                    **** "
        echo ""

        sleep $SPAWN_DELAY
done